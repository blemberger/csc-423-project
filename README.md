# read_csv.py

### Prerequisites
1. Python 2.7 installed and present in the PATH
2. **mlbgame** module installed: `pip install mlbgame`
### Running
To generate the output CSV file, from the `python` directory run:
```
python read_csv.py ../csv/MLB_all_home.csv ../csv/teams.csv
```

The two arguments are:
1) The input CSV containing all wins & losses for all seasons
2) A CSV file containing a list of all 30 MLB teams with home city timezone offsets from EDT

You can substitute a different file for 1), but if the column layout is different, you will need to modify the script, since the script references columns in this file by column number

# Running R scripts

### Prerequisites
1. R installed and present in the PATH

To run a particular script outside of RStudio:
```
r -f <script-file>.R --no-save --no-restore-data --silent
```

# Full Clean of Raw Data
To take the full dataset of downloaded games from Baseball Reference, `MLB_all.csv`, which include:

* Home games and Away games
* Date column in "Monday Jul 16" text format
* Unsorted

1) Run the following R script from **R** directory:

```
r -f clean_raw_MLB.R --no-save --no-restore-data --silent
```
  This produces the file `MLB_visiting_sorted.csv` in the **csv** directory.

2) Run the read.csv python script on this file from the **python** directory:
```
python csv_alldata_converter.py ../csv/MLB_sorted.csv
```
  This produces the file `MLB_sorted-out.csv` in the **csv** directory.
