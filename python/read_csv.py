import csv, sys, datetime, re
import mlbgame

# Regex pattern for (x)
GAME_DATE_PATTERN = re.compile(r"\w+ (\w{3} \d{1,2})(\s\((\d)\))?")
GAME_DURATION_PATTERN = re.compile(r"(\d\d?):(\d\d)")

def read_teams(team_file_name):
  with open(team_file_name, 'rb') as team_file:
    team_reader = csv.DictReader(team_file, skipinitialspace=True)
    teams = [{'abbrev': row['Abbreviation'],
              'name': row['Brief Name'],
              'timezone': row['Timezone'],
              'offset': row['EDT Offset']} for row in team_reader]
    return teams

def find_team_by_abbrev(abbrev, teams):
    for team in teams:
      if team['abbrev'] == abbrev:
        matched_team = team
        return matched_team

# We assume date_str is of the form "Friday Apr 20" and year is of the form "2012"
def parse_date(date_str, year):
  (date_str, game_num) = GAME_DATE_PATTERN.match(date_str).group(1, 3)
  full_date = "%s %s" % (date_str, year)
  date = datetime.datetime.strptime(full_date, "%b %d %Y").date() 
  return (date, game_num)

def parse_edt_time(raw_time):
  time = datetime.datetime.strptime(raw_time, "%I:%M%p")
  return time

# Return a time duration given as, e.g. "3:14" in minutes
def game_duration_minutes(duration_str):
  (hours, minutes) = GAME_DURATION_PATTERN.match(duration_str).groups()
  return int(hours)*60 + int(minutes)

def minutes_past_local_noon(time, local_offset):
  return (time.hour + local_offset - 12)*60 + time.minute

def print_teams(tms):
    for team in tms:
      print(team['abbrev'], team['name'], team['timezone'])

# Begin main
input_filename = sys.argv[1]
output_filename = input_filename + ".out"
team_filename = sys.argv[2] 
print 'input filename: %s\noutput filename: %s\nteam filename: %s' \
  % (input_filename, output_filename, team_filename)

# Read all MLB teams with home field timezones from team_filename
teams = read_teams(team_filename)
#print_teams(teams)

# open input_filename for reading in row by row, and open output_filename for writing out
with open(input_filename, 'rb') as csvinfile, open(output_filename, 'wb') as csvoutfile:
  gamereader = csv.reader(csvinfile, skipinitialspace=True)
  gamewriter = csv.writer(csvoutfile)

  # Write out the header as the first row
  in_header_row = gamereader.next()
  header_row = in_header_row[1:2] # Game #
  header_row += in_header_row[2:4] # Year, Date
  header_row += [in_header_row[4]] # Tm (Away)
  header_row += in_header_row[7:11] # Opp (Home), W_L, R, RA
  header_row += [in_header_row[18]] # Time
  header_row += ["GameTimezone"] # Append the Timezone column to the output file
  header_row += ["StartTimeEDT"] # Append game start time (all times in EDT)
  header_row += ["Home_Team_MPBN"] # Home team's minutes past biological noon
  header_row += ["Visiting_Team_MPBN"] # Visiting team's minutes past biological noon
  header_row += ["Differential_Team_MPBN"] # Home_Team_MPBN - Visiting_Team_MPBN
  header_row += ["Game_Duration_Minutes"] # Duration of the game in minutes
  header_row += ["Prev_Games_In_Timezone"] # How many previous games in the current timezone
  header_row += ["Home_Win"] # 1 for home team winning, 0 for a loss
  gamewriter.writerow(header_row)

  prev_row = None
  game_in_timezone = 0
  
  # Write to the output file for each row read from intput file (except for those
  # rows having dates we cannot parse)
  for row in gamereader:
    # Home team of each game is a team abbreviation
    home_team = find_team_by_abbrev(row[7], teams)

    # Visiting team of each game is a team abbreviation
    visiting_team = find_team_by_abbrev(row[4], teams)

    # Get the date the game was played
    (game_date, game_num) = parse_date(row[2], row[3]) 

    # What was prev_row's date?
    if (prev_row):
      prev_home_team = find_team_by_abbrev(prev_row[7], teams)
      prev_visiting_team = find_team_by_abbrev(prev_row[4], teams)

      # Calculate game_in_timezone
      if (prev_visiting_team['abbrev'] == visiting_team['abbrev'] and prev_home_team['timezone'] == home_team['timezone']):
        # Prev game played in the same timezone
        game_in_timezone += 1
      else:
        game_in_timezone = 0
    
    # Get the games for that date for this home team from mlbgame (there can be more than one)
    games = mlbgame.day(game_date.year, game_date.month, game_date.day, home=home_team["name"])
    if (len(games) < 1):
      print "Game not found in mlbgame database: %s " % game_date
      continue
    if (game_num):
      game_num_of_day = int(game_num)
      #print "Length of games: %d" % len(games)
      if game_num_of_day <= len(games):
        game = games[game_num_of_day - 1]
      else:
        print "Game not found in mlbgame database: %s (%d)" % (game_date, game_num_of_day)
        continue
    else:
      game = games[0]
  
    out_row = row[1:2] # Game #
    out_row += row[2:4] # Year, Date
    out_row += [row[4]] # Tm (Away)
    out_row += row[7:11] # Opp (Home), W_L, R, RA
    out_row += [row[18]] # Time
    out_row += [home_team['timezone']]
    raw_start_time_edt = game.game_start_time
    home_offset = int(home_team['offset'])
    visitors_offset = int(visiting_team['offset'])
    out_row += [raw_start_time_edt]
    home_mpbn = minutes_past_local_noon(parse_edt_time(raw_start_time_edt), home_offset)
    visiting_mpbn = minutes_past_local_noon(parse_edt_time(raw_start_time_edt), visitors_offset)
    out_row += [home_mpbn]
    out_row += [visiting_mpbn]
    out_row += [home_mpbn - visiting_mpbn]
    out_row += [game_duration_minutes(row[18])]
    out_row += [game_in_timezone]
    game_result = row[8]
    out_row += [0] if (game_result.startswith("W")) else [1] # 'W' indicates visitor win
    gamewriter.writerow(out_row)

    # save this row as prev_row
    prev_row = row
