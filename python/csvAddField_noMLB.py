'''
Created on Oct 21, 2016

@author: Dominic
'''

import csv, re
from datetime import datetime

def dateToNum(valIn):
    if valIn == "Jan":
        return 1
    elif valIn == "Feb":
        return 2
    elif valIn == "Mar":
        return 3
    elif valIn == "Apr":
        return 4
    elif valIn == "May":
        return 5
    elif valIn == "Jun":
        return 6
    elif valIn == "Jul":
        return 7
    elif valIn == "Aug":
        return 8
    elif valIn == "Sep":
        return 9
    elif valIn == "Oct":
        return 10
    elif valIn == "Nov":
        return 11
    elif valIn == "Dec":
        return 12

def read_teams(team_file_name):
  with open(team_file_name, 'rb') as team_file:
    team_reader = csv.DictReader(team_file, skipinitialspace=True)
    teams = [{'abbrev': row['Abbreviation'],
              'name': row['Brief Name'],
              'timezone': row['Timezone'],
              'offset': row['EDT Offset']} for row in team_reader]
    return teams

def find_team_by_abbrev(abbrev, teams):
    for team in teams:
      if team['abbrev'] == abbrev:
        matched_team = team
        return matched_team

team_filename = 'src/main/date/teams.csv' 
# Read all MLB teams with home field timezones from team_filename
teams = read_teams(team_filename)

with open('src/main/date/MLB_all_home.csv') as rcsvfile:
    reader = csv.DictReader(rcsvfile)
    with open('src/main/date/MLB_all_home_convert.csv', 'wb') as wcsvfile:
#       header_row += ["StartTimeEDT"] # Append game start time (all times in EDT)
#       header_row += ["Home_Team_MPBN"] # Home team's minutes past biological noon
#       header_row += ["Visiting_Team_MPBN"] # Visiting team's minutes past biological noon
#       header_row += ["Differential_Team_MPBN"] # Home_Team_MPBN - Visiting_Team_MPBN
        reader.fieldnames.append("Timezone_Offset")
        reader.fieldnames.append("Home_Win%")
        reader.fieldnames.append("Prev_Games_In_Series")  # How many previous games in the current series
        reader.fieldnames.append("Home_Win")  # 1 for home team winning, 0 for a loss
        writer = csv.DictWriter(wcsvfile, fieldnames=reader.fieldnames)
        writer.writeheader()
        
        prev_row = None
        game_in_series = 0
  
        for row in reader:
            
            # Home team of each game is a team abbreviation in column 7
            home_team = find_team_by_abbrev(row["Tm"], teams)
            
            # Visiting team of each game is a team abbreviation in column 9
            visiting_team = find_team_by_abbrev(row["Opp"], teams)
            
            if (prev_row):
                if (prev_row["Tm"] == row["Tm"] and prev_row["Opp"] == row["Opp"]):
                    # Same two teams played in previous record
                    game_in_series += 1
                else:
                    game_in_series = 0
            row['Prev_Games_In_Series'] = game_in_series
            winLoss = row['W-L']
            wlSplit = winLoss.split("-")
            win = 0
            loss = 0
            try:
                win = int(wlSplit[0])
            except:
                win = dateToNum(wlSplit[0])
            try:
                loss = int(wlSplit[1])
            except:
                loss = dateToNum(wlSplit[1])
            if row['W_L'].startswith('L'):
                loss = loss - 1
                row['Home_Win'] = 0
            else:
                win = win - 1
                row['Home_Win'] = 1
            total = win + loss
            if total == 0:
                row['Home_Win%'] = 0
            else:
                row['Home_Win%'] = win / float(total)
                
            if "+" in row['Streak']:
                row['Streak'] = row['Streak'].count("+")-1
            else:
                row['Streak'] = -1*(row['Streak'].count("-")-1)
                    
            tempDate = row['Date'] + " " + row['Year']
            regexDate = re.sub('(\(+\d+\))', '', tempDate).lstrip()
            f_date = datetime.strptime(regexDate, '%b %d %Y').strftime("%m/%d/%Y")
            row['Full Date'] = f_date
            
            home_offset = int(home_team['offset'])
            visitors_offset = int(visiting_team['offset'])
            row['Timezone_Offset'] = home_offset-visitors_offset
            
            writer.writerow(row)
            prev_row = row
