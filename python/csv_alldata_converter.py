import csv, sys, re
from datetime import datetime
import mlbgame

# Regex pattern for (x)
GAME_DATE_PATTERN = re.compile(r"\w+ (\w{3} \d{1,2})(\s\((\d)\))?")
GAME_DURATION_PATTERN = re.compile(r"(\d\d?):(\d\d)")

def read_teams(team_file_name):
    with open(team_file_name, 'rb') as team_file:
        team_reader = csv.DictReader(team_file, skipinitialspace=True)
        teams = [{'abbrev': row['Abbreviation'],
                  'name': row['Brief Name'],
                  'timezone': row['Timezone'],
                  'offset': row['EDT Offset']} for row in team_reader]
        return teams

def find_team_by_abbrev(abbrev, teams):
    for team in teams:
        if team['abbrev'] == abbrev:
            matched_team = team
            return matched_team

# We assume date_str is of the form "Friday Apr 20" and year is of the form "2012"
def parse_date(date_str, year):
    (date_str, game_num) = GAME_DATE_PATTERN.match(date_str).group(1, 3)
    full_date = "%s %s" % (date_str, year)
    date = datetime.strptime(full_date, "%b %d %Y").date()
    return (date, game_num)

def parse_edt_time(raw_time):
    time = datetime.strptime(raw_time, "%I:%M%p")
    return time

# Return a time duration given as, e.g. "3:14" in minutes
def game_duration_minutes(duration_str):
  (hours, minutes) = GAME_DURATION_PATTERN.match(duration_str).groups()
  return int(hours)*60 + int(minutes)

def minutes_past_local_noon(time, local_offset):
  return (time.hour + local_offset - 12)*60 + time.minute

def print_teams(tms):
    for team in tms:
      print(team['abbrev'], team['name'], team['timezone'])
      
def dateToNum(valIn):
    if valIn == "Jan":
        return 1
    elif valIn == "Feb":
        return 2
    elif valIn == "Mar":
        return 3
    elif valIn == "Apr":
        return 4
    elif valIn == "May":
        return 5
    elif valIn == "Jun":
        return 6
    elif valIn == "Jul":
        return 7
    elif valIn == "Aug":
        return 8
    elif valIn == "Sep":
        return 9
    elif valIn == "Oct":
        return 10
    elif valIn == "Nov":
        return 11
    elif valIn == "Dec":
        return 12

class Vividict(dict):
    def __missing__(self, key):
        value = self[key] = type(self)()
        return value

def read_team_payroll(team_file_name):
    payroll = Vividict()
    with open(team_file_name, 'rb') as team_file:
        team_reader = csv.DictReader(team_file, skipinitialspace=True)
        for row in team_reader:
            payroll[row["Tm"]][row['Year']] = int(row['Payroll']) 
    return payroll


# Begin main
input_filename = sys.argv[1]
output_filename = input_filename.replace(".csv", "-out.csv")
team_filename = 'teams.csv'
if(len(sys.argv)>2):
    team_filename = sys.argv[2] 
payroll_filename = 'payroll.csv'
if(len(sys.argv)>3):
    payroll_filename = sys.argv[3] 
print 'input filename: %s\noutput filename: %s\nteam filename: %s' \
  % (input_filename, output_filename, team_filename)

# Read all MLB teams with home field timezones from team_filename
teams = read_teams(team_filename)
payroll = read_team_payroll(payroll_filename)

TeamStatsDict = Vividict()
#make a dictionary of all the teams win loss records and win/loss streaks
#this uses the autovivification method recreated in python
with open(input_filename) as rcsvfile:
    reader = csv.DictReader(rcsvfile, skipinitialspace=True)
    previousStreak = 0
    prevTeam = ""
    prevYear = ""
    for row in reader:
        winLoss = row['W-L']
        wlSplit = winLoss.split("-")
        win = 0
        loss = 0
        try:
          win = int(wlSplit[0])
        except:
          win = dateToNum(wlSplit[0])
        try:
          loss = int(wlSplit[1])
        except:
            # this is actually the wins column... in these instances
          loss = win
          win = dateToNum(wlSplit[1])
        if row['W_L'].startswith('L'):
            loss = loss - 1
        else:
            win = win - 1
        total = win + loss

        tempDate = row['Date'] + " " + row['Year']
        regexDate = re.sub('(\(+\d+\))', '', tempDate).lstrip()
        f_date = datetime.strptime(regexDate, '%A %b %d %Y').strftime("%m/%d/%Y")
        row['Full Date'] = f_date

        
        TeamStatsDict[row["Tm"]][row['Full Date']]["WinLoss"] = "%d-%d" % (win, loss)

        if total == 0:
            TeamStatsDict[row["Tm"]][row['Full Date']]["WinP"] = 0
            TeamStatsDict[row["Tm"]][row['Full Date']]["Streak"] = 0
            previousStreak = 0
        else:
            TeamStatsDict[row["Tm"]][row['Full Date']]["WinP"] = win / float(total)
            TeamStatsDict[row["Tm"]][row['Full Date']]["Streak"] = previousStreak

        if "+" in row['Streak']:
            previousStreak = row['Streak'].count("+")
        else:
            previousStreak = -1 * (row['Streak'].count("-"))
        


with open(input_filename) as rcsvfile:
    reader = csv.DictReader(rcsvfile)
    with open(output_filename, 'wb') as wcsvfile:
        reader.fieldnames.append('Full Date')
        reader.fieldnames.append('Home_Streak')
        reader.fieldnames.append('Away_Streak')
        reader.fieldnames.append("WinLoss")
        reader.fieldnames.append("Home_Win_Pct")
        reader.fieldnames.append("Away_Win_Pct")
        reader.fieldnames.append("Differential_Win_Pct")
        reader.fieldnames.append("Home_Win")  # 1 for home team winning, 0 for a loss
        reader.fieldnames.append("StartTimeEDT") # Append game start time (all times in EDT)
        reader.fieldnames.append("Home_Team_MPBN") # Home team's minutes past biological noon
        reader.fieldnames.append("Visiting_Team_MPBN") # Visiting team's minutes past biological noon
        reader.fieldnames.append("Differential_Team_MPBN") # Home_Team_MPBN - Visiting_Team_MPBN
        reader.fieldnames.append("Game_Duration_Minutes") # Game duration converted to minutes
        reader.fieldnames.append("Prev_Games_In_Timezone") # Number of games spent in this timezone
        reader.fieldnames.append("Home_Team_Payroll") # Home team's payroll of that year
        reader.fieldnames.append("Visiting_Team_Payroll") # Visiting team's payroll for that year
        reader.fieldnames.append("Differential_Team_Payroll") # Home - Away Payroll
        writer = csv.DictWriter(wcsvfile, fieldnames=reader.fieldnames)
        writer.writeheader()
        prev_row = None
        game_in_timezone = 0

        for row in reader:
            this_team = find_team_by_abbrev(row["Tm"], teams)
            opposing_team = find_team_by_abbrev(row["Opp"], teams)
            home_team = opposing_team if row["Home_Away"] == "@" else this_team
            visiting_team = this_team if row["Home_Away"] == "@" else opposing_team

            (game_date, game_num) = parse_date(row["Date"], row["Year"])

            if (prev_row):
                prev_this_team = find_team_by_abbrev(prev_row["Tm"], teams)
                prev_opp_team = find_team_by_abbrev(prev_row["Opp"], teams)

                # Calculate game_in_timezone
                if (this_team == prev_this_team):
                  prev_timezone = prev_opp_team["timezone"] if prev_row["Home_Away"] == "@" else prev_this_team["timezone"] 
                  this_timezone = opposing_team["timezone"] if row["Home_Away"] == "@" else this_team["timezone"] 
                  if this_timezone == prev_timezone:
                    game_in_timezone += 1 
                  else:
                    game_in_timezone = 0
                else:
                  game_in_timezone = 0

            row["Prev_Games_In_Timezone"] = game_in_timezone

            # Get the games for that date for this team from mlbgame (there can be more than one)
            games = mlbgame.day(game_date.year, game_date.month, game_date.day, home=home_team["name"])
 
            if (len(games) < 1):
                print "Game not found in mlbgame database: %s %s %s %s " % (game_date.year, game_date.month, game_date.day, this_team["name"])
                continue
            if (game_num):
                game_num_of_day = int(game_num)
                if game_num_of_day <= len(games):
                    game = games[game_num_of_day - 1]
                else:
                    print "Game not found in mlbgame database: %s (%d)" % (game_date, game_num_of_day)
                    continue
            else:
                game = games[0]

            tempDate = row['Date'] + " " + row['Year']
            regexDate = re.sub('(\(+\d+\))', '', tempDate).lstrip()
            f_date = datetime.strptime(regexDate, '%A %b %d %Y').strftime("%m/%d/%Y")
            row['Full Date'] = f_date

            if(row["Home_Away"] == "@"):
               row['Home_Win_Pct'] = TeamStatsDict[row["Opp"]][row['Full Date']]["WinP"]
               row['Away_Win_Pct'] = TeamStatsDict[row["Tm"]][row['Full Date']]["WinP"] 
            else:
               row['Home_Win_Pct'] = TeamStatsDict[row["Tm"]][row['Full Date']]["WinP"]
               row['Away_Win_Pct'] = TeamStatsDict[row["Opp"]][row['Full Date']]["WinP"]

            row['Differential_Win_Pct'] = row['Home_Win_Pct'] - row['Away_Win_Pct']
            
            row['WinLoss'] = TeamStatsDict[row["Tm"]][row['Full Date']]["WinLoss"]

            if row['W_L'].startswith('W'):
                row['Home_Win'] = 0 if row["Home_Away"] == "@" else 1
            else:
                row['Home_Win'] = 1 if row["Home_Away"] == "@" else 0

            row["Home_Streak"] = TeamStatsDict[row["Tm"]][row['Full Date']]["Streak"]
            row["Away_Streak"] = TeamStatsDict[row["Opp"]][row['Full Date']]["Streak"]

            raw_start_time_edt = game.game_start_time
            home_offset = int(home_team['offset'])
            visitors_offset = int(visiting_team['offset'])
            row["StartTimeEDT"] = raw_start_time_edt
            home_mpbn = minutes_past_local_noon(parse_edt_time(raw_start_time_edt), home_offset)
            visiting_mpbn = minutes_past_local_noon(parse_edt_time(raw_start_time_edt), visitors_offset)
            row["Home_Team_MPBN"] = home_mpbn
            row["Visiting_Team_MPBN"] = visiting_mpbn
            row["Differential_Team_MPBN"] = home_mpbn - visiting_mpbn
            
            row["Home_Team_Payroll"] = payroll[row["Tm"]][row['Year']]
            row["Visiting_Team_Payroll"] = payroll[row["Opp"]][row['Year']]
            row["Differential_Team_Payroll"] = payroll[row["Tm"]][row['Year']] - payroll[row["Opp"]][row['Year']]

            row["Game_Duration_Minutes"] = game_duration_minutes(row["Time"])

            writer.writerow(row)
            prev_row = row
