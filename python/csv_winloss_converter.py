'''
Created on Oct 21, 2016

@author: Dominic
'''


import csv

with open('src/main/date/MLB_all_home.csv') as rcsvfile:
    reader = csv.DictReader(rcsvfile)
    with open('src/main/date/MLB_all_home_convert.csv', 'wb') as wcsvfile:
        reader.fieldnames.append("Win %")
        writer = csv.DictWriter(wcsvfile, fieldnames=reader.fieldnames)
        writer.writeheader()
        for row in reader:
            winLoss = row['W-L']
            wlSplit = winLoss.split("-")
            win = 0
            loss = 0
            try:
                win = int(wlSplit[0])
            except:
                if wlSplit[0] == "Jan":
                    win = 1
                elif wlSplit[0] == "Feb":
                    win = 2
                elif wlSplit[0] == "Mar":
                    win = 3
                elif wlSplit[0] == "Apr":
                    win = 4
                elif wlSplit[0] == "May":
                    win = 5
                elif wlSplit[0] == "Jun":
                    win = 6
                elif wlSplit[0] == "Jul":
                    win = 7
                elif wlSplit[0] == "Aug":
                    win = 8
                elif wlSplit[0] == "Sep":
                    win = 9
                elif wlSplit[0] == "Oct":
                    win = 10
                elif wlSplit[0] == "Nov":
                    win = 11
                elif wlSplit[0] == "Dec":
                    win = 12
            try:
                loss = int(wlSplit[1])
            except:
                if wlSplit[1] == "Jan":
                    loss = 1
                elif wlSplit[1] == "Feb":
                    loss = 2
                elif wlSplit[1] == "Mar":
                    loss = 3
                elif wlSplit[1] == "Apr":
                    loss = 4
                elif wlSplit[1] == "May":
                    loss = 5
                elif wlSplit[1] == "Jun":
                    loss = 6
                elif wlSplit[1] == "Jul":
                    loss = 7
                elif wlSplit[1] == "Aug":
                    loss = 8
                elif wlSplit[1] == "Sep":
                    loss = 9
                elif wlSplit[1] == "Oct":
                    loss = 10
                elif wlSplit[1] == "Nov":
                    loss = 11
                elif wlSplit[1] == "Dec":
                    loss = 12
            if row['W_L'] == 'L' :
                loss = loss - 1
            else:
                win = win - 1
            total = win + loss
            if total == 0:
                row['Win %'] = 0
            else:
                row['Win %'] = win / float(total)
            writer.writerow(row)
