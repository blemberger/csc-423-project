import csv
import re
from datetime import datetime

with open('src/main/date/MLB_all_home.csv') as rcsvfile:
    reader = csv.DictReader(rcsvfile)
    with open('src/main/date/MLB_all_home_convert.csv', 'wb') as wcsvfile:
        writer = csv.DictWriter(wcsvfile, fieldnames=reader.fieldnames)
        writer.writeheader()
        for row in reader:
            tempDate = row['Date']+ " " + row['Year']
            regexDate = re.sub('(\(+\d+\))', '', tempDate).lstrip()
            f_date = datetime.strptime(regexDate, '%b %d %Y').strftime("%m/%d/%Y")
            row['Full Date'] = f_date
            writer.writerow(row)