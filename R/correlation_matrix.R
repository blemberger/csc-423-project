# Generate Pearson R correlation matrix of Differential MPBN (Home MPBN - Visitor MPBN), Game Duration,
#     and whether the home team wins or not.

games = read.csv(file="~/course/CSC-423-Gemmell/project/repo/csv/MLB_all_home.csv.out")

differential_MPBN = games$Differential_Team_MPBN
game_duration = games$Game_Duration_Minutes
home_win =  games$Home_Win
game_data = cbind(differential_MPBN, game_duration, home_win)

correlation_matrix = cor(game_data)
round(correlation_matrix, 4)
