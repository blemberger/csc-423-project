# Home Field Advantage %
threshold <- 0.536

# Load games
games <- read.csv(file="../csv/MLB_sorted-out.csv")

#De-dupe - consider home games only
games <- games[games$Home_Away !="@",]

# Include only the columns corresponding to explanatory or response variables
game_data <- data.frame(Game_Duration_Minutes=games$Game_Duration_Minutes,
                  Home_Win=games$Home_Win)

## Game_Duration only Model

      # Create an 80/20 training-testing split
      ind <- sample(2, nrow(game_data), replace=TRUE, prob=c(0.8, 0.2))
      train <- game_data[ind==1,]
      test <- game_data[ind==2,]
      
      model.game_duration <- glm(Home_Win ~ Game_Duration_Minutes,
                     data=game_data,
                     family=binomial())
      print(summary(model.game_duration))
      
      # Make predictions on test data
      predictions <- predict(model.game_duration,newdata=test,type='response')
      predictions <- ifelse(predictions > threshold, 1, 0) # Home field advantage
      predictions <- as.factor(predictions)
      classification_success <- mean(predictions == test$Home_Win)
      print(classification_success)

## Create Logit Plot for Model:
      library(popbio)
      logi.hist.plot(train$Game_Duration_Minutes,
                     train$Home_Win,
                     boxp=FALSE,
                     type="hist",
                     col="gray",
                     int=10,
                     mainlabel = "Game Duration vs. Probability of Home Win",
                     ylabel2 = "Wins/Losses",
                     xlabel = "Game Duration (Minutes)")
      
# Draw horizontal line corresponding to win % = threshold
      abline(h=threshold,  lty=2)
      text(x=250, y=0.57, paste("p =", threshold), cex = 0.75)
      
# Draw vertical line corresponding to duration at which win % is predicted to be threshhold
      cc <- coef(model.game_duration)
      y <- log(threshold/(1-threshold))
      duration_threshold <- (y-cc[[1]])/cc[[2]]
      abline(v=duration_threshold)
      text(duration_threshold + 20, 0.25, paste("x =", round(duration_threshold, 2)), cex = 0.75)
      
