# Read in raw MLB_all data downloaded from baseball reference
MLB_all <- read.csv(file="../csv/MLB_all.csv", check.names=FALSE)
# Sort by Year, then team, then game #
MLB_sorted <- MLB_all[order(MLB_all$Year, MLB_all$Tm, MLB_all$ Game),]
# Write out sorted dataframe to a new CSV file
write.csv(MLB_sorted, file="../csv/MLB_sorted.csv", row.names=FALSE)
